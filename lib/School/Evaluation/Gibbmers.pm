package School::Evaluation::Gibbmers;

use strict;
use warnings;
# No guarantee given, use at own risk and will
# ABSTRACT: create an evaluation poll over local network

1;

__END__

=encoding utf8

=head1 SYNOPSIS

This is a stub module for cpan indexing.
No funcionality implemented.
See L<https://www.perl.com/article/how-to-upload-a-script-to-cpan/> for the reason.

You find the implementation in the script L<gibbmers>.
A later release might transfer some logic from the script to here.
